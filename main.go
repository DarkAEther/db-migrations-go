package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/DarkAEther/db-migrations-go/config"
	"gitlab.com/DarkAEther/db-migrations-go/drivers"
)

func main() {
	cfgFilePath := flag.String("cfg", "/config/cfg.yaml", "Path to the configuration file")
	shouldMigrateUp := flag.Bool("up", true, "Sets migration direction to up if true or down if false")
	targetVersion := flag.Int("version", -1, "Target Schema version. By default migrates all the way up or all the way down")
	flag.Parse()

	fmt.Println("Starting DB Migrations Go")

	config, err := config.AppConfig(*cfgFilePath)
	if err != nil {
		fmt.Printf("[FATAL] Failed to load configuration file due to error %v\n", err)
	}

	drv, err := drivers.GetMigrationDriver(config)
	if err != nil {
		fmt.Printf("[FATAL] Failed to load migration driver due to error %v\n", err)
		os.Exit(1)
	}

	res, err := drv.InitialiseMigration()
	if err != nil {
		fmt.Printf("[FATAL] Failed to initialize migration due to error %v\n", err)
	}

	fmt.Printf("Current schema version is %d and latest available is %d\n", res.CurrentSchemaVersion, res.LatestAvailableSchemaVersion)
	if *shouldMigrateUp {
		if *targetVersion == -1 {
			*targetVersion = res.LatestAvailableSchemaVersion
		}
		err = drv.Up(*targetVersion)
		if err != nil {
			fmt.Printf("[ERROR] Failed to complete database migration due to error %v\n", err)
		}
	} else {
		if *targetVersion == -1 {
			*targetVersion = 0
		}
		err = drv.Down(*targetVersion)
		if err != nil {
			fmt.Printf("[ERROR] Failed to complete database migration due to error %v\n", err)
		}
	}
}

# db-migrations-go

`db-migrations-go` is an application that, given a set of versioned migration scripts, applies the scripts incrementally until the required version is reached.

## Features
### Migrate Up
Migrates the database schema upwards to newer versions.

### Migrate Down
Migrates the database schema downward to older versions.

### Migration Fault Tolerance
If a typo or otherwise, larger error creeps into a migration script causing an error, the migration run stops at the last successful upgrade/downgrade.

Each version is applied under a transaction so all the previous migrations are safely committed.

## Database Support
Supports only postgresql in SSL Disabled mode at the moment. Support for SSL mode and other databases is on the way.

## Usage
1. Compile the application with `go build` to get a binary.
2. Create a configuration file with the details of your database and your scripts using the following format:
    ```yaml
    ---
    database:
    driver: "postgresql"
    host: "IP of the DB server"
    port: <Port of the DB server - integer>
    username: "DB username"
    password: "DB password"
    dbname: "DB name"
    scripts:
    path: "Absolute or Relative path to your scripts"
    ```
3. Create a migration script pair such as:
    ```
    0000001_test_v1.up.sql
    0000001_test_v1.down.sql
    ```
up will have SQL to upgrade the DB and down will have SQL to downgrade the DB.

4. Run the executable with command-line options to upgrade:

    ```bash
    database-migrations-go -cfg="./testscripts" -up=true -version=5
    ```

    Similarly, to downgrade:
    ```bash
    database-migrations-go -cfg="./testscripts" -up=false -version=1
    ```

## Command line Args
```
./db-migrations-go -help             
Usage of ./db-migrations-go:
  -cfg string
        Path to the configuration file (default "/config/cfg.yaml")
  -up
        Sets migration direction to up if true or down if false (default true)
  -version int
        Target Schema version. By default migrates all the way up or all the way down (default -1)
```
package drivers

import (
	"fmt"

	"gitlab.com/DarkAEther/db-migrations-go/config"
	"gitlab.com/DarkAEther/db-migrations-go/drivers/driverintf"
	"gitlab.com/DarkAEther/db-migrations-go/drivers/postgresql"
)

func GetMigrationDriver(cfg *config.Config) (driverintf.MigrationDriver, error) {
	fmt.Printf("Migration Driver: %s\n", cfg.Database.Driver)
	if cfg.Database.Driver == "postgresql" {
		pgc := postgresql.PostgresqlConfig{
			Host:     cfg.Database.Host,
			Port:     cfg.Database.Port,
			Username: cfg.Database.Username,
			Password: cfg.Database.Password,
			DbName:   cfg.Database.DbName,
		}
		drv, err := postgresql.NewPostgresqlMigrationDriver(&pgc, cfg.Scripts.Path)
		if err != nil {
			return nil, err
		}
		return drv, nil
	} else {
		fmt.Printf("[ERROR] Migration Driver %s is not supported at this time", cfg.Database.Driver)
	}
	return nil, nil
}

package driverintf

type MigrationDriver interface {
	InitialiseMigration() (*MigrationMetadata, error)
	Up(tgtVer int) error
	Down(tgtVer int) error
}

type MigrationFile struct {
	Version  int
	FileName string
}

type MigrationMetadata struct {
	AvailableSchemaVersions      map[int]MigrationFile
	LatestAvailableSchemaVersion int
	CurrentSchemaVersion         int
}

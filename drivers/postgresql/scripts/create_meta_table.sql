CREATE TABLE db_migrations_meta (
    id INT NOT NULL,
    schema_version INT NOT NULL,
    last_updated DATE NOT NULL
);

INSERT INTO db_migrations_meta
(id, schema_version, last_updated)
VALUES (0, 0, NOW());
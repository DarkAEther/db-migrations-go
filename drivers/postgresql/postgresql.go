package postgresql

import (
	"database/sql"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"

	_ "embed"

	_ "github.com/lib/pq"
	"gitlab.com/DarkAEther/db-migrations-go/drivers/driverintf"
	"golang.org/x/exp/maps"
)

type PostgresqlMigrationDriver struct {
	Db          *sql.DB
	ScriptsPath string
}

type PostgresqlConfig struct {
	Host     string
	Port     int
	Username string
	Password string
	DbName   string
}

//go:embed scripts/create_meta_table.sql
var metaCreateQuery string

func NewPostgresqlMigrationDriver(pgc *PostgresqlConfig, scriptsPath string) (driverintf.MigrationDriver, error) {
	pgConfigString := fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable", pgc.Username, pgc.Password, pgc.Host, pgc.Port, pgc.DbName)
	dbase, err := sql.Open("postgres", pgConfigString)

	if err != nil {
		return nil, err
	}

	err = dbase.Ping()

	if err != nil {
		return nil, err
	}

	md := PostgresqlMigrationDriver{
		Db:          dbase,
		ScriptsPath: scriptsPath,
	}
	return md, nil
}

func (p PostgresqlMigrationDriver) InitialiseMigration() (*driverintf.MigrationMetadata, error) {
	m := &driverintf.MigrationMetadata{}
	// check for metadata table
	exists, err := p.doesMetaTableExist()
	if err != nil {
		return nil, err
	}
	if !exists {
		fmt.Println("Migration Metadata table not found. Creating it.")
		err = p.createMetaTable()
		if err != nil {
			return nil, err
		}
	}

	currVer, err := p.getDatabaseSchemaVersion()
	if err != nil {
		return nil, err
	}
	m.CurrentSchemaVersion = currVer

	migrations, err := p.loadAvailableSchemaVersions()
	if err != nil {
		return nil, err
	}
	m.AvailableSchemaVersions = migrations

	vers := maps.Keys(migrations)
	sort.Ints(vers)
	m.LatestAvailableSchemaVersion = vers[len(vers)-1]
	return m, nil
}

func (p PostgresqlMigrationDriver) doesMetaTableExist() (bool, error) {
	rows, err := p.Db.Query("SELECT * FROM information_schema.tables WHERE table_name='db_migrations_meta'")
	if err != nil {
		return false, err
	}
	defer func() {
		err := rows.Close()
		if err != nil {
			fmt.Printf("Error while trying to close rows %v", err)
		}
	}()

	res := rows.Next()
	return res, rows.Err()
}

func (p PostgresqlMigrationDriver) createMetaTable() error {
	_, err := p.Db.Exec(metaCreateQuery)
	return err
}

func (p PostgresqlMigrationDriver) getDatabaseSchemaVersion() (int, error) {
	rows, err := p.Db.Query("SELECT schema_version FROM db_migrations_meta LIMIT 1")
	if err != nil {
		return -1, err
	}
	defer func() {
		err := rows.Close()
		if err != nil {
			fmt.Printf("Error while trying to close rows %v", err)
		}
	}()

	if !rows.Next() {
		return -1, rows.Err()
	}

	var ver int
	err = rows.Scan(&ver)

	if err != nil {
		return -1, err
	}
	return ver, nil
}

func setSchemaVersionInDb(tx *sql.Tx, version int) error {
	res, err := tx.Exec(fmt.Sprintf("UPDATE db_migrations_meta SET schema_version='%d' WHERE id='0'", version))
	if err != nil {
		return err
	}
	rowsUpdated, errAffected := res.RowsAffected()
	if errAffected != nil {
		return errAffected
	}
	if rowsUpdated < 1 {
		return fmt.Errorf("failed to set schema version in db due to unknown error")
	}
	return nil
}

func (p PostgresqlMigrationDriver) loadAvailableSchemaVersions() (map[int]driverintf.MigrationFile, error) {
	scripts, err := os.ReadDir(p.ScriptsPath)
	scriptmap := make(map[int]driverintf.MigrationFile)
	if err != nil {
		return nil, err
	}

	reVer := regexp.MustCompile("^[0-9]+")
	reName := regexp.MustCompile(`^[0-9]+_[a-zA-Z0-9-_]+\.`)
	for _, scr := range scripts {
		if !scr.Type().IsRegular() {
			continue
		}
		scrVer := reVer.FindString(scr.Name())
		scrName := reName.FindString(scr.Name())
		if scrVer == "" || scrName == "" {
			continue
		}
		ver, err := strconv.Atoi(scrVer)
		if err != nil {
			fmt.Printf("[ERROR] Found unrecognized schema version file %s\n", scr.Name())
		} else {
			fmt.Printf("Discovered available schema version %d\n", ver)
		}
		_, ok := scriptmap[ver]
		if !ok {
			scriptmap[ver] = driverintf.MigrationFile{
				Version:  ver,
				FileName: scrName,
			}
		}
	}

	return scriptmap, nil
}

func (p PostgresqlMigrationDriver) Up(tgtVer int) error {
	m, err := p.InitialiseMigration()
	if err != nil {
		return err
	}

	if tgtVer < m.CurrentSchemaVersion {
		return fmt.Errorf("target version %d is older than current schema version %d", tgtVer, m.CurrentSchemaVersion)
	}
	if tgtVer == m.CurrentSchemaVersion {
		fmt.Printf("Schema version %d already matches the target version. No migration needed.\n", m.CurrentSchemaVersion)
		return nil
	}

	keys := maps.Keys(m.AvailableSchemaVersions)
	sort.Ints(keys)

	for nextVer := m.CurrentSchemaVersion + 1; nextVer <= tgtVer; nextVer++ {
		tx, err := p.Db.Begin()
		if err != nil {
			return err
		}
		defer func() {
			rollback_err := tx.Rollback()
			if rollback_err != nil {
				fmt.Printf("[ERROR] Failed to rollback transaction due to error %v", err)
			}
		}()

		fmt.Printf("[UP] Migrating Schema from version %d to %d\n", m.CurrentSchemaVersion, nextVer)
		migFile := m.AvailableSchemaVersions[nextVer]
		err = p.loadFileAndExecute(tx, &migFile, true)
		if err != nil {
			return err
		}
		ver_update_err := setSchemaVersionInDb(tx, nextVer)
		if ver_update_err != nil {
			return ver_update_err
		}
		err = tx.Commit()
		if err != nil {
			return err
		}
		m.CurrentSchemaVersion = nextVer
	}
	return nil
}

func (p PostgresqlMigrationDriver) Down(tgtVer int) error {
	m, err := p.InitialiseMigration()
	if err != nil {
		return err
	}

	if tgtVer > m.CurrentSchemaVersion {
		return fmt.Errorf("target version %d is newer than current schema version %d", tgtVer, m.CurrentSchemaVersion)
	}
	if tgtVer == m.CurrentSchemaVersion {
		fmt.Printf("Schema version %d already matches the target version. No migration needed.\n", m.CurrentSchemaVersion)
		return nil
	}

	keys := maps.Keys(m.AvailableSchemaVersions)
	sort.Slice(keys, func(i, j int) bool {
		return i > j
	})

	for nextVer := m.CurrentSchemaVersion - 1; nextVer >= tgtVer; nextVer-- {
		tx, err := p.Db.Begin()
		if err != nil {
			return err
		}
		defer func() {
			rollback_err := tx.Rollback()
			if rollback_err != nil {
				fmt.Printf("[ERROR] Rollback failed due to error %v", err)
			}
		}()

		fmt.Printf("[DOWN] Migrating Schema from version %d to %d\n", m.CurrentSchemaVersion, nextVer)
		migFile := m.AvailableSchemaVersions[nextVer+1]
		err = p.loadFileAndExecute(tx, &migFile, false)
		if err != nil {
			return err
		}
		ver_update_err := setSchemaVersionInDb(tx, nextVer)
		if ver_update_err != nil {
			return ver_update_err
		}
		err = tx.Commit()
		if err != nil {
			return err
		}
		m.CurrentSchemaVersion = nextVer
	}
	return nil
}

func (p PostgresqlMigrationDriver) loadFileAndExecute(tx *sql.Tx, m *driverintf.MigrationFile, isUp bool) error {
	mode := "up"
	if !isUp {
		mode = "down"
	}
	filename := fmt.Sprintf("%s/%s%s.sql", p.ScriptsPath, m.FileName, mode)
	fmt.Printf("Processing file %s\n", filename)

	fileContents, err := os.ReadFile(filename)
	if err != nil {
		return err
	}

	_, err = tx.Exec(string(fileContents))
	if err != nil {
		return err
	}
	return nil
}

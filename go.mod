module gitlab.com/DarkAEther/db-migrations-go

go 1.20

require (
	github.com/lib/pq v1.10.9
	golang.org/x/exp v0.0.0-20230905200255-921286631fa9
	gopkg.in/yaml.v3 v3.0.1
)

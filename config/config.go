package config

import (
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	Database struct {
		Driver   string `yaml:"driver"`
		Host     string `yaml:"host"`
		Port     int    `yaml:"port"`
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		DbName   string `yaml:"dbname"`
	} `yaml:"database"`
	Scripts struct {
		Path string `yaml:"path"`
	} `yaml:"scripts"`
}

func AppConfig(path string) (*Config, error) {
	config := &Config{}

	file, err := os.Open(path)

	if err != nil {
		return nil, err
	}

	settings := yaml.NewDecoder(file)
	err = settings.Decode(config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
